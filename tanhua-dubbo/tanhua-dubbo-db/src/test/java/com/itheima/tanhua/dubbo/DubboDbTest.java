package com.itheima.tanhua.dubbo;

import com.itheima.tanhua.dubbo.api.UserServiceImpl;
import com.itheima.tanhua.model.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author itheima
 * @since 2022-03-03
 */
@SpringBootTest
public class DubboDbTest {

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void test() {
        User userByPhone = userService.getUserByPhone("13606000000");
    }
}
