package com.itheima.tanhua.dubbo.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.crypto.SecureUtil;
import com.github.pagehelper.PageHelper;
import com.itheima.commons.holder.TokenHolder;
import com.itheima.tanhua.dubbo.mappers.*;
import com.itheima.tanhua.model.domain.Question;
import com.itheima.tanhua.model.domain.Settings;
import com.itheima.tanhua.model.domain.User;
import com.itheima.tanhua.model.domain.UserInfo;
import com.itheima.tanhua.model.vo.BlackListItemVo;
import com.itheima.tanhua.model.vo.BlackListVo;
import com.itheima.tanhua.model.vo.SettingsVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Slf4j
@DubboService
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private BlackListMapper blackListMapper;

    @Autowired
    private SettingsMapper settingsMapper;

    @Autowired
    private QuestionMapper questionMapper;

    @Override
    public User getUserByPhone(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return null;
        }

        return userMapper.getUserByPhone(phone);
    }

    @Override
    public User createUserByPhone(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return null;
        }

        User user = new User();
        user.setMobile(phone);
        user.setPassword(SecureUtil.md5(phone + "TANHUA_PASSWORD_SALT"));
        user.setCreated(new Date());
        user.setUpdated(new Date());

        // 返回值代表 本次sql影响的行数
        int result = userMapper.insertUser(user);

        return result == 1 ? user : null;
    }

    @Override
    public UserInfo getUserInfoById(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }

        return userInfoMapper.getUserInfoById(id);
    }

    @Override
    public List<UserInfo> getUserInfoInIdList(List<String> userIdList) {
        if (CollUtil.isEmpty(userIdList)) {
            return new ArrayList<>();
        }

        return userInfoMapper.getUserInfoInIdList(userIdList);
    }

    @Override
    public UserInfo createUserInfo(UserInfo userInfo) {
        if (Objects.isNull(userInfo)) {
            return null;
        }

        int result = userInfoMapper.insertUserInfo(userInfo);
        return result == 1 ? userInfo : null;
    }

    @Override
    public Boolean updateUserInfo(UserInfo userInfo) {
        if (Objects.isNull(userInfo)) {
            return false;
        }

        return userInfoMapper.updateUserInfo(userInfo) > 0;
    }

    @Override
    public BlackListVo getBlackList(Integer page, Integer pageSize, String userId) {
        BlackListVo vo = new BlackListVo();
        // 组装分页条件
        // limit (页码-1) * 每页显示条数,  每页显示条数
        Integer skip = (page - 1) * pageSize;
        // 查询分页数据
        List<BlackListItemVo> blackList = blackListMapper.getBlackList(skip, pageSize, userId);
        // 查询满足条件的总记录数
        Long amount = blackListMapper.getBlackAmount(userId);

        // 计算页数
        Long total = amount / pageSize;
        if (amount % pageSize != 0) {
            total++;
        }

        // 组装并返回
        vo.setItems(blackList);
        vo.setCounts(amount.intValue());
        vo.setPage(page);
        vo.setPages(total.intValue());
        vo.setPagesize(pageSize);

        return vo;
    }

    @Override
    public Boolean updateOrCreateQuestion(String content, String userId) {
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(content)) {
            log.warn("updateOrCreateQuestion入参为空");
            return false;
        }

        // 检测问题是否存在
        Question checkQuestionExists = questionMapper.selectQuestionByUserId(userId);

        // 组装问题数据
        Question question = new Question();
        question.setTxt(content);
        question.setUserId(userId);
        question.setUpdated(new Date());

        // 如果问题不存在，则创建
        if (Objects.isNull(checkQuestionExists)) {
            question.setCreated(new Date());
            return questionMapper.insertQuestion(question) > 0;
        }

        // 如果问题已存在，则更新
        return questionMapper.updateQuestion(question) > 0;
    }

    @Override
    public Boolean removeBlackListByUserId(String userId, String blackUserId) {
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(blackUserId)) {
            return false;
        }

        return blackListMapper.deleteBlackList(userId, blackUserId) > 0;
    }

    @Override
    public SettingsVo getSettingsByUserId(String userId) {
        SettingsVo vo = new SettingsVo();
        vo.setGonggaoNotification(true);
        vo.setLikeNotification(true);
        vo.setPinglunNotification(true);

        // SELECT * FROM tb_user_info ui
        // LEFT JOIN tb_settings s ON ui.id = s.user_id
        // LEFT JOIN tb_question q ON ui.id = q.user_id

        Question question = questionMapper.selectQuestionByUserId(userId);
        if (Objects.nonNull(question)) {
            BeanUtils.copyProperties(question, vo);
        }

        Settings settings = settingsMapper.selectSettingsByUserId(userId);
        if (Objects.nonNull(settings)) {
            BeanUtils.copyProperties(settings, vo);
        }

        return vo;
    }

    @Override
    public Boolean updateSettingByUserId(Settings settings) {
        return null;
    }

}
