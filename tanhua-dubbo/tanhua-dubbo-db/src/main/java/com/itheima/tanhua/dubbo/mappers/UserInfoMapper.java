package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.User;
import com.itheima.tanhua.model.domain.UserInfo;
import com.itheima.tanhua.model.dto.RecommendationDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
public interface UserInfoMapper {

    /**
     * 根据id获取用户信息
     *
     * @param id 用户id
     * @return 用户信息
     */
    UserInfo getUserInfoById(String id);

    /**
     * 根据id列表获取用户信息
     *
     * @param idList id列表
     * @return 用户信息列表
     */
    List<UserInfo> getUserInfoInIdList(@Param("idList") List<String> idList);

    /**
     * 插入用户信息
     *
     * @param userInfo 用户信息
     * @return 插入结果
     */
    int insertUserInfo(UserInfo userInfo);

    /**
     * 修改用户信息
     *
     * @param userInfo 用户信息
     * @return 修改结果
     */
    int updateUserInfo(UserInfo userInfo);

    /**
     * 随机获取一个同城用户
     *
     * @param userId 当前用户id（用于排除）
     * @param city   城市
     * @return UserInfo
     */
    UserInfo randomUserByCity(@Param("userId") String userId, @Param("city") String city);

    /**
     * 获取推荐用户列表
     *
     * @param userId 当前用户id（用于排除）
     * @param skip   偏移量
     * @param limit  每页显示条数
     * @param dto    查询条件
     * @return List<UserInfo>
     */
    List<UserInfo> selectUsersByCity(@Param("userId") String userId,
                                     @Param("skip") Integer skip,
                                     @Param("limit") Integer limit,
                                     @Param("city") String city,
                                     RecommendationDto dto);
}
