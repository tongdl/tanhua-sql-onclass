package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.Friend;
import com.itheima.tanhua.model.vo.BlackListItemVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
public interface FriendMapper {

    /**
     * 获取朋友列表
     *
     * @param userId 用户编号
     * @return List
     */
    List<Friend> getFriendsByUserId(@Param("userId") String userId);

}
