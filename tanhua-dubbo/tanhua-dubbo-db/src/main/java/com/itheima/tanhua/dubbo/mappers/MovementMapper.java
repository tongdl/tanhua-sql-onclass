package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.Movement;
import com.itheima.tanhua.model.domain.Settings;
import com.itheima.tanhua.model.vo.MovementVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
@Resource
public interface MovementMapper {

    /**
     * 插入动态
     *
     * @param movement 动态信息
     * @return 插入结果
     */
    int insertMovement(Movement movement);

    /**
     * 获取推荐动态列表
     *
     * @param skip     偏移量
     * @param pageSize 每页显示条数
     * @return List<MovementVo>
     */
    List<MovementVo> getRecommendList(@Param("skip") Integer skip, @Param("pageSize") Integer pageSize);
}
