package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.Question;
import com.itheima.tanhua.model.vo.BlackListItemVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
public interface QuestionMapper {

    /**
     * 插入问题
     *
     * @param question 问题内容
     * @return 更新结果
     */
    int insertQuestion(Question question);

    /**
     * 更新问题
     *
     * @param question 问题内容
     * @return 更新结果
     */
    int updateQuestion(Question question);

    /**
     * 根据用户id查询问题
     *
     * @param userId 用户id
     * @return 问题内容
     */
    Question selectQuestionByUserId(String userId);


}
