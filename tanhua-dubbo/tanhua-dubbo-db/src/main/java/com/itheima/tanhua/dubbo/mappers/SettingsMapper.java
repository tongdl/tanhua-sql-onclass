package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.Settings;
import com.itheima.tanhua.model.vo.BlackListItemVo;
import com.itheima.tanhua.model.vo.SettingsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
public interface SettingsMapper {

    /**
     * 查找用户系统设置
     *
     * @param userId 用户id
     * @return 系统设置信息
     */
    Settings selectSettingsByUserId(@Param("userId") String userId);
}
