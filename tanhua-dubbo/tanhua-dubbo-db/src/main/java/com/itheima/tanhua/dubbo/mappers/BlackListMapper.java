package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.User;
import com.itheima.tanhua.model.vo.BlackListItemVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
public interface BlackListMapper {

    /**
     * 获取黑名单列表
     *
     * @param skip     偏移值
     * @param pageSize 每页显示条数
     * @param userId   用户编号
     * @return List
     */
    List<BlackListItemVo> getBlackList(@Param("skip") Integer skip, @Param("pageSize") Integer pageSize, @Param("userId") String userId);

    /**
     * 获取黑名单总数
     *
     * @param userId 用户编号
     * @return 总数
     */
    Long getBlackAmount(@Param("userId") String userId);

    /**
     * 删除黑名单
     *
     * @param userId 用户id
     * @return 删除结果
     */
    int deleteBlackList(@Param("userId") String userId, @Param("blackUserId") String blackUserId);
}
