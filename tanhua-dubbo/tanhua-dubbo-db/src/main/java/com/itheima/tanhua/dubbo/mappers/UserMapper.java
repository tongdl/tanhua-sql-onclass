package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
public interface UserMapper {

    /**
     * 根据手机号查询用户
     *
     * @param phone 手机号
     * @return 用户信息
     */
    User getUserByPhone(String phone);

    /**
     * 根据id查询用户
     *
     * @param id 用户id
     * @return 用户信息
     */
    User getUserById(String id);

    /**
     * 插入用户信息
     *
     * @param user 用户信息
     * @return 插入结果
     */
    int insertUser(User user);
}
