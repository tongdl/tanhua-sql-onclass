package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.MovementLikeLog;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.Resource;

/**
 * @author itheima
 * @since 2022-03-12
 */
@Mapper
@Resource
public interface MovementLikeLogMapper {

    /**
     * 插入日志
     *
     * @param log 日志
     * @return 插入结果
     */
    int insertLog(MovementLikeLog log);
}
