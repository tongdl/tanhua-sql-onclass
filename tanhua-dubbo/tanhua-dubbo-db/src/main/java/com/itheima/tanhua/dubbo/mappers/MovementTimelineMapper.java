package com.itheima.tanhua.dubbo.mappers;

import com.itheima.tanhua.model.domain.Movement;
import com.itheima.tanhua.model.domain.MovementTimeline;
import com.itheima.tanhua.model.vo.MovementVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Mapper
@Resource
public interface MovementTimelineMapper {

    /**
     * 插入时间线
     *
     * @param timeline 时间线信息
     * @return 插入结果
     */
    int insertMovementTimeline(MovementTimeline timeline);
}
