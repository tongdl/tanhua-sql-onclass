package com.itheima.tanhua.dubbo.api;

import com.itheima.tanhua.dubbo.mappers.UserInfoMapper;
import com.itheima.tanhua.model.domain.UserInfo;
import com.itheima.tanhua.model.dto.RecommendationDto;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-06
 */
@DubboService
public class TanhuaServiceImpl implements TanhuaService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo randomUserByCity(String userId, String city) {
        return userInfoMapper.randomUserByCity(userId, city);
    }

    @Override
    public List<UserInfo> findRecommendUserList(String userId, String city, RecommendationDto dto) {
        // 组装分页条件
        Integer page = dto.getPage();
        Integer pageSize = dto.getPagesize();
        Integer skip = (page - 1) * pageSize;

        return userInfoMapper.selectUsersByCity(userId, skip, pageSize, city, dto);
    }
}
