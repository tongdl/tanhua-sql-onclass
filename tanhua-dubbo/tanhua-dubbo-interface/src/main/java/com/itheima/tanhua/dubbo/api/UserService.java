package com.itheima.tanhua.dubbo.api;

import com.itheima.tanhua.model.domain.Question;
import com.itheima.tanhua.model.domain.Settings;
import com.itheima.tanhua.model.domain.User;
import com.itheima.tanhua.model.domain.UserInfo;
import com.itheima.tanhua.model.vo.BlackListVo;
import com.itheima.tanhua.model.vo.SettingsVo;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-03
 */
public interface UserService {
    /**
     * 根据手机号查询用户
     *
     * @param phone 手机号
     * @return 用户信息
     */
    User getUserByPhone(String phone);

    /**
     * 创建用户
     *
     * @param phone 手机号
     * @return 用户信息
     */
    User createUserByPhone(String phone);

    /**
     * 根据id获取用户信息
     *
     * @param id 用户id
     * @return 用户信息
     */
    UserInfo getUserInfoById(String id);

    /**
     * 根据id列表获取用户信息
     *
     * @param userIdList id列表
     * @return 用户信息列表
     */
    List<UserInfo> getUserInfoInIdList(List<String> userIdList);

    /**
     * 创建用户
     *
     * @param userInfo 用户信息
     * @return 用户信息
     */
    UserInfo createUserInfo(UserInfo userInfo);

    /**
     * 更新用户信息
     *
     * @param userInfo 用户信息
     * @return 更新结果
     */
    Boolean updateUserInfo(UserInfo userInfo);


    /**
     * 获取黑名单列表
     *
     * @param page     页码
     * @param pageSize 每页显示条数
     * @param userId   用户编号
     * @return List
     */
    BlackListVo getBlackList(Integer page, Integer pageSize, String userId);

    /**
     * 更新问题
     *
     * @param question 问题内容
     * @return 更新结果
     */
    Boolean updateOrCreateQuestion(String content, String userId);

    /**
     * 根据用户id删除黑名单
     *
     * @param userId 用户id
     * @return 删除结果
     */
    Boolean removeBlackListByUserId(String userId, String blackUserId);

    /**
     * 获取用户的系统设置
     *
     * @param userId 用户id
     * @return 系统设置信息
     */
    SettingsVo getSettingsByUserId(String userId);


    /**
     * 更新提醒设置
     *
     * @param settings 提醒设置信息
     * @return 更新结果
     */
    Boolean updateSettingByUserId(Settings settings);

}
