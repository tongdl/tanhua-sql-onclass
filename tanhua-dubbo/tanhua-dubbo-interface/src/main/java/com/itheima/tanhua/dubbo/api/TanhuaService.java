package com.itheima.tanhua.dubbo.api;

import com.itheima.tanhua.model.domain.UserInfo;
import com.itheima.tanhua.model.dto.RecommendationDto;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-06
 */
public interface TanhuaService {

    /**
     * 随机获取用户（根据城市）（并排除某个用户）
     *
     * @param userId 用户id
     * @param city   城市
     * @return 用户信息
     */
    UserInfo randomUserByCity(String userId, String city);


    /**
     * 获取推荐列表
     * @param userId
     * @param dto
     * @return
     */
    List<UserInfo> findRecommendUserList(String userId, String city, RecommendationDto dto);
}
