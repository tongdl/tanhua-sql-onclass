package com.itheima.tanhua.dubbo.api;

import com.itheima.tanhua.model.domain.Movement;
import com.itheima.tanhua.model.domain.MovementLikeLog;
import com.itheima.tanhua.model.domain.UserInfo;
import com.itheima.tanhua.model.dto.RecommendationDto;
import com.itheima.tanhua.model.vo.MovementVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-09
 */
public interface MovementService {

    /**
     * 发布动态
     *
     * @param movement 动态
     * @return 发布结果
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean sendMovement(Movement movement);

    /**
     * 获取推荐动态列表
     * <p>
     * 我现在要做什么？查什么数据？往什么表里面写数据？
     * 想不出来：重新来过，从需求那边在熟悉
     * <p>
     * 我查询数据的条件是什么？
     * <p>
     * 我的返回结果要包含什么？
     *
     * @param page     页码
     * @param pageSize 每页显示条数
     * @return List<MovementVo>
     */
    List<MovementVo> getRecommendList(Integer page, Integer pageSize);

    /**
     * 插入日志
     *
     * @param log 日志
     * @return 插入结果
     */
    Boolean addLikeLog(MovementLikeLog log);
}
