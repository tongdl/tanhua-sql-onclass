package com.itheima.tanhua.autoconfig.properties;

/**
 * @author itheima
 * @since 2021-12-09
 */
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "tanhua.oss")
public class OssProperties {
    private String accessKey;

    private String secret;

    private String bucketName;

    private String url;

    private String endpoint;
}