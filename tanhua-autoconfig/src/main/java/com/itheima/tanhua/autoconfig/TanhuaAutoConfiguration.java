package com.itheima.tanhua.autoconfig;

/**
 * @author itheima
 * @since 2021-12-09
 */

import com.itheima.tanhua.autoconfig.properties.OssProperties;
import com.itheima.tanhua.autoconfig.properties.SmsProperties;
import com.itheima.tanhua.autoconfig.template.OssTemplate;
import com.itheima.tanhua.autoconfig.template.SmsTemplate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * 自动装配类
 *
 * @author itheima
 */
@EnableConfigurationProperties({
        SmsProperties.class,
        OssProperties.class
})
public class TanhuaAutoConfiguration {

    @Bean
    public SmsTemplate smsTemplate(SmsProperties properties) {
        return new SmsTemplate(properties);
    }

    @Bean
    public OssTemplate ossTemplate(OssProperties ossProperties) {
        return new OssTemplate(ossProperties);
    }
}