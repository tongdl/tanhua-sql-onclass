package com.itheima.commons.holder;

import cn.hutool.core.collection.CollUtil;

import java.util.Map;
import java.util.Objects;

/**
 * @author itheima
 * @since 2022-03-05
 */
public class TokenHolder {
    private static ThreadLocal<Map<String, String>> tl = new ThreadLocal<>();

    public static Map<String, String> get() {
        return tl.get();
    }

    public static void set(Map<String, String> value) {
        tl.set(value);
    }


    public static String getPhone() {
        Map<String, String> map = TokenHolder.get();
        if (CollUtil.isEmpty(map)) {
            return null;
        }

        return map.get("phone");
    }

    public static String getUserId() {
        Map<String, String> map = TokenHolder.get();
        if (CollUtil.isEmpty(map)) {
            return "";
        }

        return map.get("userId");
    }

    public static void remove() {
        tl.remove();
    }
}
