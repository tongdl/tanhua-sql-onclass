package com.itheima.commons.constant;

/**
 * @author itheima
 * @since 2022-03-15
 */
public class RedisConstant {
    public final static String MOVEMENT_LIKE_HASH_KEY = "movement_like_";

    public final static String RECOMMENT_MOVEMENT_KEY = "recommendMovement";
}
