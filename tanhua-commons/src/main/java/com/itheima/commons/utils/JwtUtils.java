package com.itheima.commons.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.Map;

public class JwtUtils {

    /**
     * TOKEN的有效期1小时（S）
     */
    private static final int TOKEN_TIME_OUT = 1 * 3600;

    /**
     * 加密KEY
     */
    private static final String TOKEN_SECRET = "itcast";

    /**
     * 生成Token
     *
     * @param params Token的主体部分（要签名的内容）
     * @return token
     */
    public static String getToken(Map<String, Object> params) {
        long currentTime = System.currentTimeMillis();

        return Jwts.builder()
                // 加密方式
                // 加密算法：HS512
                // 加密key
                // 对 'zhangsan' 进行加密
                // String 加密后的结果 = HS512(zhangsan+itcats)
                .signWith(SignatureAlgorithm.HS512, TOKEN_SECRET)
                // 过期时间戳（token过期时间）
                .setExpiration(new Date(currentTime + TOKEN_TIME_OUT * 1000))
                // 添加到载荷里面的数据
                .addClaims(params)
                .compact();
    }

    /**
     * 获取Token中的claims信息
     *
     * @param token token
     * @return Claims claims信息
     */
    public static Claims getClaims(String token) {
        return Jwts.parser()
                .setSigningKey(TOKEN_SECRET)
                .parseClaimsJws(token).getBody();
    }

    /**
     * 检测token是否有效
     *
     * @param token 需检测的token
     * @return true-有效，false-失效
     */
    public static boolean verifyToken(String token) {
        if (StringUtils.isEmpty(token)) {
            return false;
        }

        // 试着解析内容
        // 如果内容解析错误，会抛异常，则返回false
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey("itcast")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}