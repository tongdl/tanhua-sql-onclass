# 探花SQL版-课堂版

## 介绍


### 任务10：动态+阿里云审核

### 任务11：使用JMeter测试接口性能


### 任务12：使用Redis优化朋友圈点赞

#### app-server项目

- controller.MovementController.likeByRedis（点赞）
- controller.MovementController.dislikeByRedis（取消点赞）
- task.RedisTask（定时任务，用于隔一段时间将Redis数据同步给MySQL）

#### dubbo-db项目

- api.MovementServiceImpl.addLikeLog
- mappers.MovementLikeLogMapper.insertLog

#### 表结构

【新增】点赞记录表

```sql
create table tanhua.tb_movement_like_log
(
	id int auto_increment
		primary key,
	movement_id int null comment '动态id',
	user_id int null comment '用户id',
	type varchar(10) null comment 'add点赞,reduce取消点赞',
	created datetime null,
	updated datetime null
)
comment '点赞记录表';
```

## 任务13：使用RabbitMQ优化朋友圈发送资讯接口

### app-server

- controller.MovementController.sendMovementByMq

### dubbo-db

- mq.SpringRabbitListener
- api.MovementServiceImpl.sendMovement


## 任务14：借助RabbitMQ实现短信群发
假设有一个新需求，需要你开发一个短信群发接口。

需求：
1. 找到系统中半年内没有登录的用户；
2. 向这些用户发送召回短信/邮件；
3. 短信/邮件发送结果需要记录到MySQL中；
4. 需注意短时间大量发送给数据库带来的压力；
5. 涉及的表结构需自己改造或新建；

## 任务15：实现接口根据喜欢匹配用户