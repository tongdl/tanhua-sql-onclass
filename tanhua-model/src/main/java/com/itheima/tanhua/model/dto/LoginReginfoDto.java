package com.itheima.tanhua.model.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author itheima
 * @since 2022-03-05
 */
@Data
public class LoginReginfoDto {
    private String avatar;
    private String nickname;
    private String birthday;
    private Integer gender;
    private Integer age;
    private String city;
    private String income;
    private String education;
    private String profession;
    private String marriage;
    private String tags;
    private String coverPic;
    private Date created;
    private Date updated;
}
