package com.itheima.tanhua.model.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-03-04
 */
@Data
public class LoginDto {
    private String phone;
}
