package com.itheima.tanhua.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author itheima
 * @since 2022-03-06
 */
@Data
public class SettingsVo implements Serializable {
    private Integer id;
    private String strangerQuestion;
    private String phone;
    private Boolean likeNotification;
    private Boolean pinglunNotification;
    private Boolean gonggaoNotification;
}
