package com.itheima.tanhua.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-11
 */
@Data
public class MovementVo implements Serializable {
    private String id;
    private String userId;
    private String avatar;
    private String nickname;
    private String gender;
    private Integer age;
    private List<String> tags;
    private String textContent;
}
