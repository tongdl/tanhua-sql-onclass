package com.itheima.tanhua.model.vo;

import com.itheima.tanhua.model.domain.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-06
 */
@Data
public class BlackListVo implements Serializable {
    private Integer counts;
    private Integer pagesize;
    private Integer pages;
    private Integer page;
    private List<BlackListItemVo> items;
}
