package com.itheima.tanhua.model.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-03-09
 */
@Data
public class LocationDto {
    private Double latitude;
    private Double longitude;
    private String addrStr;
    private String userId;
    private Integer radius;
}
