package com.itheima.tanhua.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author itheima
 * @since 2022-03-06
 */
@Data
public class BlackListItemVo implements Serializable {
    private String id;
    private String avatar;
    private String nickname;
    private String gender;
    private Integer age;
}
