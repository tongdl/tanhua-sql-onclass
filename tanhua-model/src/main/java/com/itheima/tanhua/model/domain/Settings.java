package com.itheima.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通知设置
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Settings extends BasePojo {

    private String id;
    private String userId;
    private Boolean likeNotification;
    private Boolean pinglunNotification;
    private Boolean gonggaoNotification;
}