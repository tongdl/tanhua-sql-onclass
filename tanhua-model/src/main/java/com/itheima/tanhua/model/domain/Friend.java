package com.itheima.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Friend extends BasePojo {

    /**
     * 主键id
     */
    private String id;

    private String userId;

    private String friendId;

    private Date created;
}