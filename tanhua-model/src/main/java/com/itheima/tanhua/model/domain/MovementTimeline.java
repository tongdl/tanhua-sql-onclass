package com.itheima.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovementTimeline extends BasePojo {

    /**
     * 主键id
     */
    private String id;

    private String movementId;

    private String userId;

    private String friendId;
}