package com.itheima.tanhua.model.dto;

import lombok.Data;

import java.io.InputStream;
import java.io.Serializable;

/**
 * @author itheima
 * @since 2022-03-10
 */
@Data
public class RabbitMovementMediaDto implements Serializable {
    private String contentType;
    private Long size;
    private byte[] is;
}
