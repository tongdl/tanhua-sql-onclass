package com.itheima.tanhua.model.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-03-09
 */
@Data
public class TestDto {
    private String name;
    private Integer age;
}
