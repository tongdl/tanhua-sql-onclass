package com.itheima.tanhua.model.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//陌生人问题
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_question")
public class Question extends BasePojo {

    private String id;
    private String userId;
    //问题内容
    private String txt;

}