package com.itheima.tanhua.model.vo;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-03-04
 */
@Data
public class LoginVerificationVo {
    private Boolean isNew;
    private String token;
}
