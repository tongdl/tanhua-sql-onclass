package com.itheima.tanhua.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 首页推荐用户列表入参
 *
 * @author itheima
 * @since 2022-02-01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecommendationDto implements Serializable {
    private Integer page;

    private Integer pagesize;

    private String gender;

    private Integer age;

    private String education;

    /**
     * 原需求中的city字段可以废弃掉了
     */
    private String city;
}