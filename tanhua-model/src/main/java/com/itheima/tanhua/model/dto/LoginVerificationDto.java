package com.itheima.tanhua.model.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-03-03
 */
@Data
public class LoginVerificationDto {
    private String phone;
    private String verificationCode;
}
