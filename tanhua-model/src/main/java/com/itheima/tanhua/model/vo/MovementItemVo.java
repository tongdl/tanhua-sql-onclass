package com.itheima.tanhua.model.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-15
 */
@Data
public class MovementItemVo {
    private String id;
    private String userId;
    private String avatar;
    private String nickname;
    private String gender;
    private Integer age;
    private List<String> tags;
    private String textContent;
    private String[] imageContent;
    private String distance;
    private Date createDate;
    private Integer likeCount;
    private Integer commentCount;
    private Integer loveCount;
    private Integer hasLiked;
    private Integer hasLoved;
}
