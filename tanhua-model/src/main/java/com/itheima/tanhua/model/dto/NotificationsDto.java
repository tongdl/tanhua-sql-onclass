package com.itheima.tanhua.model.dto;

import lombok.Data;

@Data
public class NotificationsDto {
    private Boolean likeNotification;
    private Boolean pinglunNotification;
    private Boolean gonggaoNotification;
}