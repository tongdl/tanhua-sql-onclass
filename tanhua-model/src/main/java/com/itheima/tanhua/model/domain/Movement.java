package com.itheima.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Movement extends BasePojo {

    /**
     * 主键id
     */
    private String id;

    /**
     * 用于推荐系统的模型(自动增长)
     */
    private Long pid;
    private Long userId;

    /**
     * 文字
     */
    private String textContent;

    /**
     * 媒体数据，图片或小视频 url
     */
    private List<String> medias;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 位置名称
     */
    private String locationName;

    /**
     * 状态 0：未审（默认），1：通过，2：驳回
     */
    private Integer state = 0;

    /**
     * 点赞数
     */
    private Integer likeCount = 0;

    /**
     * 评论数
     */
    private Integer commentCount = 0;

    /**
     * 喜欢数
     */
    private Integer loveCount = 0;
}