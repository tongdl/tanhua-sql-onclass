package com.itheima.tanhua.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-11
 */
@Data
public class MovementListVo implements Serializable {

    private Integer count;

    private Integer pagesize;

    /**
     * 总页数
     */
    private Integer pages;

    /**
     * 页码
     */
    private Integer page;


    private List<Object> items;

}
