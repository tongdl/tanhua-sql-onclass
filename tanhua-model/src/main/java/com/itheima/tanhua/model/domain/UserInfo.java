package com.itheima.tanhua.model.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-05
 */
@Data
public class UserInfo implements Serializable {
    private String id;
    private String avatar;
    private String nickname;
    private String birthday;
    private String gender;
    private Integer age;
    private String city;
    private String income;
    private String education;
    private String profession;
    private String marriage;
    private String tagString;

    @TableField(exist = false)
    private List<String> tags;

    private String coverPic;

    private Date created;
    private Date updated;
}
