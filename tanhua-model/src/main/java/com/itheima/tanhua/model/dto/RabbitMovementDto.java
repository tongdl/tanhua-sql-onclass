package com.itheima.tanhua.model.dto;

import com.itheima.tanhua.model.domain.Movement;
import lombok.Data;

import java.util.List;

/**
 * @author itheima
 * @since 2022-03-10
 */
@Data
public class RabbitMovementDto extends Movement {
    private List<RabbitMovementMediaDto> imageContent;
}
