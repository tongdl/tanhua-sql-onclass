package com.itheima.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 今日佳人出参
 *
 * @author itheima
 * @since 2022-02-01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TodayBestVo {
    private String id;
    private String avatar;
    private String nickname;
    private String gender;
    private Integer age;
    private List<String> tags;
    private Integer fateValue;
}