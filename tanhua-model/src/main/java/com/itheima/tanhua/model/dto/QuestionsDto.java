package com.itheima.tanhua.model.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-02-01
 */
@Data
public class QuestionsDto {
    private String content;
}