package com.itheima.tanhua.server.controller;

import com.itheima.commons.holder.TokenHolder;
import com.itheima.tanhua.dubbo.api.UserService;
import com.itheima.tanhua.model.domain.Question;
import com.itheima.tanhua.model.domain.Settings;
import com.itheima.tanhua.model.domain.UserInfo;
import com.itheima.tanhua.model.dto.NotificationsDto;
import com.itheima.tanhua.model.dto.QuestionsDto;
import com.itheima.tanhua.model.vo.BlackListVo;
import com.itheima.tanhua.model.vo.SettingsVo;
import feign.Param;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author itheima
 * @since 2022-03-06
 */
@RestController
@RequestMapping("/users")
public class UsersController {

    @DubboReference
    private UserService userService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * Day03 黑名单列表
     * <p>
     * 1. 如果我把瓜哥加入到黑名单，那么我的id是user_id，瓜哥的id是black_user_id
     * 2. yxl把瓜哥加入到黑名单，那么yxl的id是user_id，瓜哥的id是black_user_id
     * 3. 瓜哥把我加黑名单，user_id = 瓜哥,black_user_id = 我
     * <p>
     * 将和我产生黑名单关系的所有black_user_id查出来，然后连上user_info表，获取他们的信息
     *
     * @param page     页码
     * @param pageSize 每页显示条数
     * @return ResponseEntity
     */
    @GetMapping("/blacklist")
    public ResponseEntity getBlackList(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer pageSize) {
        String userId = TokenHolder.getUserId();
        BlackListVo blackList = userService.getBlackList(page, pageSize, userId);
        return ResponseEntity.ok(blackList);
    }

    /**
     * Day03 移除陌生人问题
     *
     * @param uid uid
     * @return ResponseEntity
     */
    @DeleteMapping("/blacklist/{uid}")
    public ResponseEntity removeBlackList(@PathVariable String uid) {
        String userId = TokenHolder.getUserId();
        Boolean aBoolean = userService.removeBlackListByUserId(userId, uid);
        return aBoolean ? ResponseEntity.ok(null) : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("删除成功");
    }

    /**
     * Day03 通知设置
     *
     * @return ResponseEntity
     */
    @PostMapping("/notifications/setting")
    public ResponseEntity updateNotificationsSetting(@RequestBody NotificationsDto dto) {
        String id = TokenHolder.getUserId();

        Settings settings = new Settings();
        settings.setUserId(id);
        BeanUtils.copyProperties(dto, settings);
        Boolean result = userService.updateSettingByUserId(settings);

        return ResponseEntity.status(result ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }

    /**
     * Day03 查询通用设置
     *
     * @return ResponseEntity
     */
    @GetMapping("/settings")
    public ResponseEntity getSetting() {
        String userId = "1";
        String phone = "13500000000";

        SettingsVo vo = userService.getSettingsByUserId(userId);
        if (Objects.isNull(vo)) {
            return ResponseEntity.ok(null);
        }
        vo.setPhone(phone);
        vo.setId(Integer.parseInt(userId));

        return ResponseEntity.ok(vo);
    }

    /**
     * Day03 设置陌生人问题
     *
     * @param dto 陌生人问题
     * @return ResponseEntity
     */
    @PostMapping("/questions")
    public ResponseEntity updateQuestions(@RequestBody QuestionsDto dto) {
        Boolean result = userService.updateOrCreateQuestion(dto.getContent(), TokenHolder.getUserId());
        return ResponseEntity.status(result ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }


    /**
     * 根据标签推荐用户
     *
     * @return 推荐用户列表
     */
    @GetMapping("/recommend/tags")
    public ResponseEntity recommendUserByTags() {
        String userId = TokenHolder.getUserId();
        UserInfo userInfo = userService.getUserInfoById(userId);
        List<String> tags = userInfo.getTags();

        Set intersect = redisTemplate.opsForSet().intersect(tags);
        System.out.println(intersect);

        return ResponseEntity.ok("");
    }

    @PostMapping("/recommand/tags")
    public ResponseEntity testTags(String tag, String userId) {
        redisTemplate.opsForSet().add(tag, userId);
        return ResponseEntity.ok("");
    }
}
