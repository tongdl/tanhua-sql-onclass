package com.itheima.tanhua.server.thread;

import java.util.concurrent.Callable;

/**
 * @author itheima
 * @since 2022-03-11
 */
public class MyCallable implements Callable<String> {
    @Override
    public String call() throws Exception {
        Thread.sleep(1000);
        return "done";
    }
}
