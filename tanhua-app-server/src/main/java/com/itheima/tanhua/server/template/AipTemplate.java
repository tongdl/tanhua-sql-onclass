package com.itheima.tanhua.server.template;

import com.baidu.aip.face.AipFace;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class AipTemplate {

    @Value("${tanhua.aip.appId}")
    private String appId;

    @Value("${tanhua.aip.apiKey}")
    private String apiKey;

    @Value("${tanhua.aip.secretKey}")
    private String secretKey;

    public boolean detect(String imageUrl) {

        AipFace client = new AipFace(appId, apiKey, secretKey);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        // 调用接口
        String imageType = "URL";

        HashMap<String, String> options = new HashMap<String, String>();
        options.put("face_field", "age");
        options.put("max_face_num", "2");
        options.put("face_type", "LIVE");
        options.put("liveness_control", "LOW");

        // 人脸检测
        // imageUrl : 上传到OSS上的图片地址
        // imageType : 上传图片的类型
        // opyion :
        JSONObject res = client.detect(imageUrl, imageType, options);
        System.out.println(res.toString(2));

        Integer error_code = (Integer) res.get("error_code");

        return error_code == 0;
    }
}
