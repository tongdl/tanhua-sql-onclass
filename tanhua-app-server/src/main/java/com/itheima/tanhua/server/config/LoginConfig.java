package com.itheima.tanhua.server.config;

import com.itheima.tanhua.server.interceptor.TokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class LoginConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(new TokenInterceptor());

        // 我要拦截哪些接口
        registration.addPathPatterns("/**");
        // 哪些接口不被拦截
        registration.excludePathPatterns(
                "/user/loginVerification",
                "/user/login"
        );
    }
}