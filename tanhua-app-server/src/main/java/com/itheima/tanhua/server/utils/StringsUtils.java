package com.itheima.tanhua.server.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author itheima
 * @since 2022-03-09
 */
public class StringsUtils {

    public static List<String> string2List(String txt) {
        if(StringUtils.isEmpty(txt)) {
            return null;
        }

        String[] split = txt.split(",");
        return Arrays.asList(split);
    }
}
