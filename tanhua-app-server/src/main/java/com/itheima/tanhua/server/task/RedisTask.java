package com.itheima.tanhua.server.task;

import com.itheima.tanhua.dubbo.api.MovementService;
import com.itheima.tanhua.model.domain.Movement;
import com.itheima.tanhua.model.domain.MovementLikeLog;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author itheima
 * @since 2022-03-11
 */
@Component
public class RedisTask {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @DubboReference
    private MovementService movementService;

    @Scheduled(fixedRate = 1000)
    public void refresh() {

    }
}
